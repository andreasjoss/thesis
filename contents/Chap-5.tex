% !TEX root = ../thesis3.tex

%\chapter{Design and Construction}
%\chapter{Design Comparison}
\chapter{Design Recommendation}
\label{chp:comparison}
\section{Introduction}\label{sec:comparison_intro}
In Chapter \ref{chp:OPTIMISATION}, the optimisation of three different machine configurations for a range of pole numbers was done. That is, the IDRFPM machine utilising Litz wire, the SORSPM machine utilising Litz wire and the SORSPM machine utilising solid copper bars were optimised in Sections \ref{sec:optimisation_results_idrfpm_round_litz}, \ref{sec:optimisation_results_sorspm_round_litz} and \ref{sec:optimisation_results_sorspm_solid_bars} respectively. At the end of each section, a motivation was given for the most appealing pole number. In this chapter, the performance and machine parameters of the top candidate for each topology is compared and discussed. Thereafter, a single design will be recommended based on the aforementioned performance comparison and based on some practical considerations. To this end, an investigation into the construction of the the novel SORSPM machine utilising copper bars is done.

\section{Design Comparison}\label{sec:design_comparison}
The top candidates from each machine configuration is compared in this section. In Figure \ref{fig:performance_comparison_cross_platform_100}, the performance comparison at rated torque, rated power is shown. At first glance, the most noticeable differences are the torque density and magnet mass. The IDRFPM machine has a significantly lower torque density of \mbox{$2.70\text{~}$\Nm/kg} while the SORSPM design utilising Litz wire is at \mbox{$7.17\text{~}$\Nm/kg}, and the design utilising solid copper bars is at \mbox{$7.29\text{~}$\Nm/kg}. This vast difference in torque density between the IDRFPM and SORSPM designs, is largely attributed to the massive difference in magnet mass. The IDRFPM machine's large magnet mass of $\SI{50.79}{kg}$ was required in order to achieve the rated torque requirement, while adhering to the copper loss budget of $\SI{200}{W}$. In this project it is especially important to achieve a high torque density, or low total mass, as the machine's mass will add to the total vehicle mass, which will theoretically increase the torque requirement for a single motor initially calculated as \mbox{$191$ \Nm}. Since it is envisioned in this project to have both the rear wheels each propelled by its own motor, the machine mass added to the vehicle is at least double the value displayed in Figure \ref{fig:performance_comparison_cross_platform_100}. Thus for the IDRFPM machine, the total mass added to the vehicle will at the very least be a staggering $\SI{141.36}{kg}$, which of course excludes other mechanical components such as the machine end-plates (back and front plates), shaft and ball bearings. In comparison, the SORSPM machine utilising solid copper bars, will add at least $\SI{52.42}{kg}$ to the total vehicle mass. The extremely large IDRFPM machine mass, would cause the vehicle's torque requirement to increase drastically, and in turn necessitate the IDRFPM machine's torque (and thus machine size) to increase yet again. This situation could very well lead to a runaway situation, in which the ever increasing vehicle mass and motor torque will prevent the vehicle's requirements to be met. To put these values into perspective, as mentioned in Section \ref{sec:vehicle_modelling}, the existing golf cart powertrain has a mass of $\SI{70}{kg}$. To be fair, the $\SI{70}{kg}$ also includes drum breaks and an axle on which the chassis can be mounted. Therefore, if a practical powertrain with the SORSPM machine is employed, the actual total powertain mass is likely to be in the vicinity of $\SI{70}{kg}$. The point is that the IDRFPM solution is very far from the aforementioned values.

Considering the rated torque, rated power operating point from Figure \ref{fig:performance_comparison_cross_platform_100}, the only performance metrics in which the IDRFPM machine excels in comparison to the other SORSPM designs, is that of the power factor, efficiency and torque ripple. While at this operating point, the IDRFPM machine's efficiency and torque ripple advantage is not as significant, the power factor is impressive as it is rounded up to unity. The almost perfect power factor is rather appealing, compared to the the power factors of $0.88$ and $0.85$ of the SORSPM designs. Recall that in Chapter \ref{chp:2}, Table \ref{tbl:motor_required_specs}, it was stated that the desired efficiency and power factor at rated (base) speed is $\SI{89}{\%}$ and $0.90$ respectively. All the candidates considered here, adhere to the $\SI{89}{\%}$ efficiency constraint. Unfortunately, the SORSPM designs do not satisfy the minimum power factor goal of $0.90$.

The performance at the rated power, top speed operating point is shown in Figure \ref{fig:performance_comparison_cross_platform_465}. Again, the IDRFPM machine's power factor, efficiency and torque ripple trumps that of the SORSPM designs.  The power factor of the IDRFPM machine remains at unity, however unlike at the rated torque operating point, here the SORSPM designs perform much worse, achieving only $0.62$ and $0.78$ respectively. This is because both SORSPM machines make use of flux weakening at the top speed operating point. Additionally, the efficiency of the IDRFPM machine is astounding, almost reaching the $\SI{100}{\%}$ mark. Keep in mind that the efficiency numbers reported in this project do not account for mechanical and friction losses. In comparison, the SORSPM designs exhibit a much lower efficiency, of only $\SI{87.38}{\%}$ and $\SI{85.94}{\%}$ for the Litz and solid bar configurations respectively. The reason for the noticeably lower efficiency of the SORSPM machine utilising solid bars, is because of the eddy losses within the copper bars during high speed operation. However, since efficiency was one of the objectives of the optimisation procedure, the optimisation process was able to protect the copper bars from excessive eddy current losses (as was the case in Table \ref{tab:bar_losses}). This is quite clear when comparing the copper location in Figure \ref{fig:SORSPM_bars_candidate} to that of the Litz variant in Figure \ref{fig:SORSPM_litz_candidate}. The copper bars are located much further from the airgap in order to avoid the alternating flux caused by the magnets.

The disadvantage of the aforementioned copper bar placement, is that it makes the machine susceptible to torque ripple at higher speeds (constant power operation) as the reluctance torque component becomes more pronounced. This can be seen when comparing the overall torque ripple results shown for the top speed in Figures \ref{fig:performance_comparison_sorspm_round_litz_465} and \ref{fig:performance_comparison_sorspm_solid_bars_465}. However, torque ripple was also an optimisation objective in Chapter \ref{chp:OPTIMISATION}, which means a compromise between efficiency and torque ripple for the copper bar topology had to be reached. Unfortunately, despite the compromise, for any given pole number both the efficiency and torque ripple is less desirable than that of the SORSPM machine utilising Litz wire. The torque ripple of the IDRFPM machine remains below $\SI{1}{\%}$, while the SORSPM machine utilising Litz wire is at $\SI{8.40}{\%}$ and the SORSPM machine utilising solid bars is at $\SI{18.23}{\%}$.

Figure \ref{fig:input_comparison_cross_platform} shows the machine parameters and dimensions of each design, while Figures \ref{fig:IDRFPM_candidate}, \ref{fig:SORSPM_litz_candidate} and \ref{fig:SORSPM_bars_candidate} show the physical representations thereof, all according to the same scale. The fact that the optimisation procedure for the IDRFPM machine opted for a fairly long stack length of $\SI{290.12}{mm}$, shows that the IDRFPM configuration struggles to provide the torque requirement within the constraints of this project. When comparing Figures \ref{fig:IDRFPM_candidate}, \ref{fig:SORSPM_litz_candidate} and \ref{fig:SORSPM_bars_candidate}, the sheer size of the IDRFPM machine's magnets, compared to the SORSPM candidates, is particularly striking. The aforementioned, together with the IDRFPM machine's long stack length, results in the very large magnet mass. The IDRFPM machine's much larger aluminium mass is also as a result of the long stack length. The IDRFPM machine and the SORSPM machine utilising Litz wire were both optimised to the outer radius limit of $\SI{135}{mm}$, while the SORSPM design utilising copper bars settled at a lower value of $\SI{128.94}{mm}$. Therefore the most compact machine is the SORSPM machine utilising copper bars with a stack length of $\SI{116.50}{mm}$, while the SORSPM machine utilising Litz wire is at second place with $\SI{120.32}{mm}$ and the IDRFPM last with $\SI{290.12}{mm}$.

\pagebreak
\begin{figure}[H]
  \begin{center}
%    \includegraphics[width=1.0\textwidth]{./figures/performance_comparison_cross_platform_100.pdf}
    \includegraphics[width=1.0\textwidth]{./figures/cross_comparison/sorspm_bars_p48/performance_comparison_cross_platform_100.pdf}
    \caption{Performance comparison of the most appealing design of each machine configuration at $\SI{2}{kW}$, $\SI{100}{rpm}$.}
    \label{fig:performance_comparison_cross_platform_100}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
%    \includegraphics[width=1.0\textwidth]{./figures/performance_comparison_cross_platform_465.pdf}
    \includegraphics[width=1.0\textwidth]{./figures/cross_comparison/sorspm_bars_p48/performance_comparison_cross_platform_465.pdf}
    \caption{Performance comparison of the most appealing design of each machine configuration at $\SI{2}{kW}$, $\SI{465}{rpm}$.}
    \label{fig:performance_comparison_cross_platform_465}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
%    \includegraphics[width=1.03\textwidth]{./figures/input_comparison_cross_platform.pdf}
    \includegraphics[width=1.03\textwidth]{./figures/cross_comparison/sorspm_bars_p48/input_comparison_cross_platform.pdf}
    \caption{Input parameter comparison of the most appealing design of each machine configuration.}
    \label{fig:input_comparison_cross_platform}
  \end{center}
\end{figure}

\newcommand\fplscaleCandidate{1.7} %using tikz clip
\newcommand\fplClipBottomCandidate{13cm}
\newcommand\fplClipTopCandidate{2.5cm}
\newcommand\fplClipLeftCandidate{6cm} %.335\width
\newcommand\fplClipRightCandidate{6cm} %.325\width

\begin{figure}[H]
 \begin{minipage}{1\textwidth}
 \centering
 %\fbox{
\adjustbox{scale=\fplscaleCandidate,trim={\fplClipLeftCandidate} {\fplClipBottomCandidate} {\fplClipRightCandidate} {\fplClipTopCandidate +0.5cm},clip}{
\adjustbox{angle=60}{
\begin{tikzpicture}
    \savebox\mysavebox{\includegraphics[width=.9\textwidth]{./figures/IDRFPM_fpl/p16.pdf}}
    \path [use as bounding box] rectangle (\wd\mysavebox, \ht\mysavebox);
	\path [clip] (\wd\mysavebox, 0.5\ht\mysavebox) arc [start angle=0, end angle=60, radius=0.5\wd\mysavebox] -- (0.5\wd\mysavebox,0.5\ht\mysavebox) -- (\wd\mysavebox,0.5\ht\mysavebox);	
    \node [anchor=south west, inner sep=0pt, outer sep=0pt] at (0,0) {\usebox\mysavebox};
\end{tikzpicture}
}}%}
  \caption{IDRFPM machine, utilising round Litz wire, 16 pole.}
  \label{fig:IDRFPM_candidate}
 \end{minipage}
 \begin{minipage}{1\textwidth}
\vspace{20pt}
  \centering
%\fbox{
\adjustbox{scale=\fplscaleCandidate,trim={\fplClipLeftCandidate} {\fplClipBottomCandidate +0.5cm} {\fplClipRightCandidate} {\fplClipTopCandidate},clip}{
\adjustbox{angle=60}{
\begin{tikzpicture}
    \savebox\mysavebox{\includegraphics[width=.9\textwidth]{./figures/SORSPM_fpl/litz/p40.pdf}}
    \path [use as bounding box] rectangle (\wd\mysavebox, \ht\mysavebox);
	\path [clip] (\wd\mysavebox, 0.5\ht\mysavebox) arc [start angle=0, end angle=60, radius=0.5\wd\mysavebox] -- (0.5\wd\mysavebox,0.5\ht\mysavebox) -- (\wd\mysavebox,0.5\ht\mysavebox);	
    \node [anchor=south west, inner sep=0pt, outer sep=0pt] at (0,0) {\usebox\mysavebox};
\end{tikzpicture}
}}%}
  \caption{SORSPM machine, utilising round Litz wire, 40 pole.}
  \label{fig:SORSPM_litz_candidate}
 \end{minipage}
 \begin{minipage}{1\textwidth}
\vspace{20pt}
  \centering
%\fbox{
\adjustbox{scale=\fplscaleCandidate,trim={\fplClipLeftCandidate} {\fplClipBottomCandidate +0.5cm} {\fplClipRightCandidate} {\fplClipTopCandidate},clip}{
\adjustbox{angle=60}{
\begin{tikzpicture}
    \savebox\mysavebox{\includegraphics[width=.9\textwidth]{./figures/SORSPM_fpl/bars/p48.pdf}}
    \path [use as bounding box] rectangle (\wd\mysavebox, \ht\mysavebox);
	\path [clip] (\wd\mysavebox, 0.5\ht\mysavebox) arc [start angle=0, end angle=60, radius=0.5\wd\mysavebox] -- (0.5\wd\mysavebox,0.5\ht\mysavebox) -- (\wd\mysavebox,0.5\ht\mysavebox);	
    \node [anchor=south west, inner sep=0pt, outer sep=0pt] at (0,0) {\usebox\mysavebox};
\end{tikzpicture}
}}%}
  \caption{SORSPM machine, utilising solid copper bars, 48 pole.}
  \label{fig:SORSPM_bars_candidate}
 \end{minipage} 
\end{figure}

\section{Construction Investigation}\label{sec:proposed_construction}
\subsection{Introduction}
In Section \ref{sec:design_comparison}, the designs were compared in terms of basic performance metrics and machine dimensions. However, a comprehensive comparison cannot be made without an investigation into the manufacturing feasibility of the SORSPM machine utilising copper bars. Working prototypes of the IDRFPM machine and the SORSPM machine utilising Litz wire already exist, so it is assumed that their manufacturability is reasonable. 

In this section, a method to construct a prototype of the SORSPM machine utilising copper bars is investigated. The investigation serves as a first attempt, and is not necessarily the best or most efficient method to construct the machine. The purpose of this section is twofold: to determine if the optimised SORSPM designs utilising copper bars could be practically implemented, and two serve as a starting point for future projects which wish to continue an investigation into the mechanical aspects of the machine.

The 3D images that follow in this section were created using Autodesk Inventor\textsuperscript{\textregistered} Professional 2016 (Student Version), and will be discussed according to a sequence of assembly steps. The figures were initially drawn for a 40 pole, 30 slot machine which was not optimised to consider the eddy losses within the copper bars. Nonetheless the proposed construction procedure remains the same. The detailed dimensions and performance metrics can be seen in Appendix \ref{app:sorspm_example_machine}. As a reminder to the reader, the prototype presented here is not meant to be an in-hub (in-wheel) prototype, but rather a direct-drive motor. It is however envisioned that future projects with a focus on the mechanical aspects, will aim to build an in-hub machine for this light-vehicle application.

\subsection{Stator}\label{sec:stator_construction}
Figure \ref{fig:copper_bars} shows the copper bars connected to the copper end-turn pieces. The copper bars have different lengths, of which the reason will become clearer later in this discussion. The end-turn pieces, placed below the copper bars, are used to create two turns per coil. The pieces have pre-drilled holes so that screws (in this case M3 brass screws) can be used to attach the pieces onto the copper bars. A single lamination is also shown in order to better visualise the stator layout. Figure \ref{fig:stator_drum} shows the aluminium stator drum, which serves as the stator yoke and end-plate.
\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_5.png}
  \caption{Copper bars, with end-turn pieces, and a single lamination shown of the machine specified in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:copper_bars}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
  \vspace{40pt}
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_6.png}
  \caption{Aluminium stator drum of the machine described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_drum}
 \end{minipage}
\end{figure}

Consider Figure \ref{fig:stator_drum_with_first_fr4_sheet}. In order to prevent eddy currents from flowing between stator laminations, transformer paper (or also commonly known as Nomex\textsuperscript{\textregistered} paper) can be wrapped around the the stator yoke. The transformer paper is indicated as a coarse white coloured material. The green ring is made from a $\SI{0.8}{mm}$ thick FR4 sheet (also discussed in Section \ref{solid_copper_bars}), and will insulate the end-turn pieces from the stator drum. The FR4 material can be purchased in sheets and then cut into shape using a Computer Numerical Control (CNC) machine. The M4 threaded nylon rods will help compress the stator lamination stack, and will also hold the copper bars and end-turn pieces in place. Nylon (specifically Nylon 66) rods are chosen, as it has a sufficiently high operating temperature and it pertains a similar magnetic permeability to air, thus not interfering with the electromagnetic features of the machine. Additionally, nylon does not conduct electrical current, which prevents potential eddy currents within the rods. At this stage, the nylon rods should be secured with nuts at the bottom of the stator drum. In Figure \ref{fig:stator_drum_with_copper_bars}, the copper bars (with end-turn pieces pre-attached to the bars) are placed one by one on top of the stator drum, with the nylon rods protruding through the M4 holes in the center of the copper end-turn pieces. The copper bars are thus now held in place by the nylon rods. Then, another FR4 ring is dropped into place, guided by the nylon rods. This FR4 ring will rest on top of the end-turn pieces, and will insulate the lamination stack from these copper end-turn pieces.
\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_9.png}
  \caption{Aluminium stator drum, with FR4 ring, transformer paper and nylon rods. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_drum_with_first_fr4_sheet}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
  \vspace{40pt}
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_11.png}
  \caption{Same as Figure \ref{fig:stator_drum_with_first_fr4_sheet}, but with copper bars placed into position. An additional FR4 ring is inserted, which will rest on top of the end-turn pieces. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_drum_with_copper_bars}
 \end{minipage}
\end{figure}

In Figure \ref{fig:stator_drum_with_third_fr4_sheet}, the laminations are all slid into place, guided by the nylon rods and the key openings on the stator drum. A final FR4 ring is added on top of the lamination stack, to once again provide insulation from the copper pieces which will be added on top. The copper bars in Figure \ref{fig:stator_drum_with_third_fr4_sheet} needs to be insulated from each other, and also insulated from the stator laminations. This insulation is provided by FR4 sheets which are inserted axially along the copper bars, as shown in Figure \ref{fig:fr4_copper_bar_isolation}. The top view is shown in Figure \ref{fig:fr4_copper_bar_isolation_b}. As can be seen from the top view, seven individual FR4 pieces are required per slot. As an additional precaution or a complete alternative, the copper bars could also have been wrapped in transformer paper. At this stage the copper bars should fit tightly within the slots. The nylon rod ends protruding at the top of the stack can also now be tightened with nuts.

\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_13.png}
  \caption{Aluminium stator drum, with the lamination stack in place, and a final FR4 ring on top. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_drum_with_third_fr4_sheet}
 \end{minipage}
 \begin{minipage}{1\textwidth}
% \begin{center}
 \centering
  \vspace{50pt}
%  \includegraphics[width=0.9\textwidth]{./figures/Inventor/Instruksies/Screenshot_11.png}
\begin{figure}[H]
\centering
%  \begin{subfigure}[b]{0.46\textwidth}
  \begin{subfigure}[b]{0.473\textwidth}
    \includegraphics[width=\textwidth]{./figures/Inventor/Instruksies/Screenshot_15.png}
    \caption{}
    \label{fig:fr4_copper_bar_isolation_a}
  \end{subfigure}
  \hfill
%  \begin{subfigure}[b]{0.42\textwidth}
  \begin{subfigure}[b]{0.43\textwidth}
    \includegraphics[width=\textwidth]{./figures/Inventor/Instruksies/Screenshot_14.png}
    \caption{}
    \label{fig:fr4_copper_bar_isolation_b}
  \end{subfigure}
  \caption{FR4 sheets inserted along with copper bars. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:fr4_copper_bar_isolation}
\end{figure}
%\end{center}
 \end{minipage}
\end{figure}

In Figure \ref{fig:stator_almost_complete_assembly}, the end-turn pieces which finally complete the two-turn coil is also mounted using M3 brass screws. It should be noted that all the end-turn pieces are designed in such a manner that the copper cross-section area remains constant throughout the current's path. Also, the bottom ball bearing is fitted into its locator. The locator can be seen in Figure \ref{fig:stator_drum}. The shaft is then located into the bottom ball bearing. The top ball bearing is placed within the stator cap (ball bearing not explicitly shown). The ball bearings are identical and is sized $\SI{25}{mm}$ and $\SI{52}{mm}$ for the inner and outer diameter respectively. Finally, the stator cap is merged with the shaft and stator assembly, and is married using six M6 steel threaded rods. The stator cap and stator drum now compress the laminations at the yoke region, while the laminations are also compressed at the stator shoes with the aid of the nylon rods. It is important to realise that the purpose of the M4 and M6 rods, is not only to compress the laminations, but also to hold the bottom end-turn pieces in a secure position against the stator drum. Since these end-turn pieces are also joined with the copper bars, the bars are also held in place as a result. If this were not the case, the bars would be free to move in the axial direction. Figure \ref{fig:stator_openings} shows the bottom side of the stator drum. These openings can also be seen in Figure \ref{fig:stator_drum}. The purpose of the openings is to prevent the screw heads from making contact with the stator drum, as the M3 brass screws will also be conducting some current. Furthermore, the screw heads of neighbouring coils are very close to one another, thus the openings could perhaps be filled with epoxy resin in order to insulate the heads from each other, and also from the stator drum itself. In fact, it will probably be best to pour epoxy resin over the entire stator structure with the help of a mould. This would help to keep all the components firmly in place and also increase the insulation between neighbouring conductors.

\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_17.png}
  \caption{Majority of stator now assembled, with end-turn pieces, stator cap, shaft and ball bearings in place. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_almost_complete_assembly}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
  \vspace{10pt}
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_18_new.png}
  \caption{Stator openings in stator drum, in order to insulate the screw heads from the stator drum and neighbouring screw heads. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_openings}
 \end{minipage}
\end{figure}

In Figure \ref{fig:stator_complete_assembly}, the series connections between each coil is added. Phase $a$ coils are represented by yellow, phase $b$ by green and phase $c$ by red. As can be seen from the figure, the coil connections are transposed. This is the reason for the different copper bar lengths. To achieve this transposition, the connections interchange in four different heights. Due to the number of symmetric sections of the machine, the transposition is not perfect but it is probably better than not transposing at all. The three-phase terminals are coloured in blue. In these figures the terminals are connected for a delta ($\Delta$) connection, but it was later on decided to implement a wye (Y) connection for this machine topology as discussed in Section \ref{sec:sorspm_stator_connections}.

It is thought, that the copper end-turn pieces and the copper series connectors of Figure \ref{fig:stator_complete_assembly} could be cut into shape also using a CNC machine. Alternatively, the series connectors could also be cut as regular straight bars, and then bent using a bending machine. Another option is to make use of flexible braided copper cables as shown in Figure \ref{fig:braided_copper_cables}. Of course, it would be necessary to insulate these cables.

\clearpage
\begin{figure}[t!]
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_19.png}
  \caption{Complete stator assembly, with series connections between coils and three-phase terminals included. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:stator_complete_assembly}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
  \vspace{40pt}
%  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_11.png}
  \includegraphics[width=0.6\textwidth]{./figures/aircooledcables5.jpg}
  \caption{The external connections could perhaps be realised using flexible braided copper cables, commonly used as earthing conductors in buildings.}
  \label{fig:braided_copper_cables}
 \end{minipage}
\end{figure}

\subsection{Rotor}\label{sec:rotor_construction}
In Figure \ref{fig:rotor_drum}, a cross-section view of the aluminium rotor drum is shown. The rotor drum is insulated from the rotor laminations, with the aid of transformer paper and a FR4 ring as shown in Figure \ref{fig:rotor_drum_insulation}. In Figure \ref{fig:rotor_complete}, the laminations and an additional FR4 ring is added. The magnets are glued into place. Finally, a rotor cap is placed on top to compress the rotor laminations. The rotor cap is married with the rotor drum using 16 M5 screws.

\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=0.9\textwidth]{./figures/Inventor/Instruksies/Screenshot_32.png}
  \caption{Aluminium rotor drum. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:rotor_drum}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
%  \vspace{10pt}
  \includegraphics[width=0.9\textwidth]{./figures/Inventor/Instruksies/Screenshot_33.png}
  \caption{Rotor drum with insulating transformer paper and FR4 ring. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:rotor_drum_insulation}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
%  \vspace{10pt}
%  \includegraphics[width=0.9\textwidth]{./figures/Inventor/Instruksies/Screenshot_36.png}
%  \caption{Rotor drum with laminations, magnets an additional FR4 ring inserted.}
%  \label{fig:rotor_drum_magnets}
  \includegraphics[width=0.9\textwidth]{./figures/Inventor/Instruksies/Screenshot_37.png}
  \caption{Complete rotor assembly of the machine in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:rotor_complete}
 \end{minipage} 
\end{figure}

\subsection{Complete Assembly}\label{sec:complete_assembly}
The rotor drum can now be merged with the stator assembly, as shown in Figure \ref{fig:machine_complete}. A cross-section view of the complete assembly is given by Figure \ref{fig:complete_cross_section}. Notice that the ball bearings are not explicitly shown. A closer look at the stator and rotor is given by the top view in Figure \ref{fig:machine_complete_top_view}.
\subsection{Conclusion}\label{sec:construction_conclusion}
It is the author's opinion that the complexity of this machine topology is much more challenging than initially anticipated. Securing the copper pieces together will likely be a very time consuming and error-prone step. Perhaps alternatively, the copper connections could instead be brazed together, although this could also be just as time consuming. Furthermore, the parts required for the end-turns and for the series connections could be very expensive to order if they need to be cut with a CNC machine. Another concern is that, due to the high number of copper parts, it could be difficult to ensure that all the copper parts are properly insulated.

In this investigation it was assumed that transposition of the phase conductors is necessary. The various copper bar lengths required at specific slots will certainly be difficult to install without making any misplacements. However, it would be interesting to see if neglecting the transposition would be noticeably detrimental to the machine's performance. If the transposition is abandoned, the series connections could be simplified substantially.

All these uncertainties are probably best answered when such a prototype is actually built. Nevertheless, after consulting with the SED workshop, the author concludes that this topology is likely too cumbersome, time-consuming and expensive to build.
\begin{figure}
 \begin{minipage}{1\textwidth}
 \centering
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_20.png}
  \caption{Complete machine assembly of the machine in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:machine_complete}
 \end{minipage}
 \begin{minipage}{1\textwidth}
 \centering
  \vspace{40pt}
  \includegraphics[width=1\textwidth]{./figures/Inventor/Instruksies/Screenshot_23.png}
  \caption{Cross-section view of the complete machine assembly. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:complete_cross_section}
 \end{minipage} 
\end{figure}

%\clearpage
\afterpage{\clearpage}
\begin{figure}[H]
%\begin{figure}[p]
%\begin{figure}[t!]
%\begin{minipage}{1\textwidth}
 \centering
%  \vspace{10pt}
  \includegraphics[width=0.8\textwidth]{./figures/Inventor/Instruksies/Screenshot_22.png}
  \caption{Top view of the stator and rotor. Machine dimensions as described in Appendix \ref{app:sorspm_example_machine}.}
  \label{fig:machine_complete_top_view}
% \end{minipage}
 \end{figure}
%\section{Design Proposal}
\section{Design Recommendation}\label{sec:design_proposal}
In this section, the design comparison in Section \ref{sec:design_comparison} and the construction investigation in Section \ref{sec:proposed_construction}, are considered in order to make a final design recommendation.

The IDRFPM machine certainly exhibits some performance traits which are very desirable at both base and top speed operating points. These traits are, the outstanding efficiency, power factor and torque ripple. However, the major drawbacks are the massive magnet mass, much longer stack length, and consequently a much lower torque density. Needless to say, the IDRFPM machine's magnet mass would be very cost-inefficient. Unfortunately these drawbacks, especially the machine's large mass value, render the IDRFPM machine infeasible for a direct-drive light vehicle application. As discussed in Section \ref{sec:design_comparison}, the IDRFPM machine's large mass could cause a runaway situation in which the increasing vehicle mass would require an even larger IDRFPM machine. Therefore a more suitable design recommendation has to be made, with the remaining two SORSPM configurations in mind.

During the earlier stages of this project, it was initially expected that the SORSPM machine utilising solid copper bars would deliver the most promising all-round results. Although, at the time, the magnitude of the eddy current losses within the copper bars was not expected to be as severe. After more thorough research on the issue was done, as presented in Section \ref{sec:SORSPM_eddy_current_losses}, it became evident that the eddy current losses within the copper bars will play a major role in the machine's performance. In Section \ref{sec:design_comparison} it is discussed that in order to avoid these eddy losses, the copper bars were optimised so that their cross-section area became smaller, and that they are located further away from the airgap. This caused the torque ripple to increase, and also a decrease in the torque density (compared to if the eddy losses within the copper bars were ignored). Therefore, the performance gains that were initially pursued with this topology, could not be achieved. Instead, the SORSPM machine utilising solid copper bars promises only a marginal performance improvement in some aspects, but at the expense of a very difficult and expensive construction process.

Table \ref{tbl:design_comparison_summary} serves as a summary of all the key points for the comparison between the SORSPM machine utilising Litz wire and the SORSPM machine utilising solid copper bars.

\begin{table}[H]
\begin{center}
\setlength{\extrarowheight}{.3em}
%\begin{tabular}{l|l}
\begin{tabular}{|p{0.48\textwidth}|p{0.48\textwidth}|}
%\begin{tabular}{p{7.5cm}|p{7.5cm}}
\hline
SORSPM Litz Wire & SORSPM Solid Bars\\
\hline\hline
\tabitem better all-round $\eta$ & \tabitem slightly better $\tau_{density}$\\
\tabitem much better $\tau_{ripple}$ at top speed & \tabitem smaller outer radius and stack length\\
\tabitem better power factor at base speed & \tabitem better power factor at top speed\\
\tabitem easier construction &\tabitem Y-connection prevents triplen harmonics\\
\hline
\end{tabular}
\end{center}
\caption{A summary of all the most important advantages of each configuration.}
\label{tbl:design_comparison_summary}
\end{table}

Finally in conclusion, of the topologies considered in this project, it is believed that the 40 pole SORSPM machine utilising round Litz wire is the best suited candidate for the direct-drive application of the Mellowcabs L2 class electrical vehicle.